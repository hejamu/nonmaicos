..
    Names are in alphabetical order

Maintainers
-----------

- Henrik Stooß

Developers
----------

- Henrik Stooß
- Kira Fischer

Contributors
------------

- Philipp Stärk